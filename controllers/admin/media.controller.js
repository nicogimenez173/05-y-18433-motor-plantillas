const express = require ("express");
const router = express.Router();
const {db, getAll} = require('../../db/conexion');
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/images' })
const fileUpload = upload.single('src');
const sqlite3 = require('sqlite3');
const path = require('path');

const mediacontroller = {
    index: async function (req, res) {
        const media = await getAll("select * from media Where estado = 1");
    console.log("media", media);

    const Tmedia = req.params.Tmedia;
    console.log(Tmedia)
    const mediaFilter=await getAll ("select * from media", [Tmedia]);
    res.render('admin/media/index',{
        media:media,
    });
    },

    create: async function (req, res) {
        res.render('admin/media/crearForm');
    },

    store: async function (req, res) {
        try {
            const { id, nombre, estado } = req.body;
            db.run(
                "INSERT INTO media (id, nombre, estado) VALUES (?, ?, ?)",
                [id, nombre, estado],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/media/listar");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },

    //show: async function (req, res) {},

    update: async function (req, res) {
        const idmedia = parseInt(req.params.idmedia);
        console.log ("idmedia", idmedia);
        if (!req.body.nombre) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        
            db.run("UPDATE media SET nombre = ? WHERE id = ?",
            [req.body.nombre, idmedia],
            (err) => {
                if (err) {
                    console.log ("error", err);                    
                }
                res.redirect('/admin/media/listar?success=' + encodeURIComponent('¡Registro modificafo correctamente!'));
            }
        );

    },

    edit: async function (req, res) {
        try {
            const idmedia = parseInt(req.params.idmedia);
            const media = await getAll('SELECT * FROM media WHERE id = ?', [idmedia]);
            if (!media) {
                return res.status(404).send('tipo media no encontrado');
            }
            res.render('admin/media/editform', {
                media: media
            });
        } catch (error) {
            console.error('Error al obtener el tipo media:', error);
            res.status(500).send('Error al obtener el tipo media');
        };
    },

    destroy: async function (req, res) {
        const idmedia = parseInt(req.params.idmedia);
        console.log ("idmedia", idmedia);
        
        db.run(
            "UPDATE media SET estado = 0 WHERE id = ?",
            [idmedia],
            (err) => {
                if (err) {
                    console.log ("error", err);                    
                }
                res.redirect('/admin/media/listar?success=' + encodeURIComponent('¡Registro eliminado correctamente!'));
            }
        );
    },
};

module.exports = mediacontroller;
