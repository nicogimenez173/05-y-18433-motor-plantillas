CREATE TABLE IF NOT EXISTS "integrantes" (
	"id" INTEGER NOT NULL UNIQUE,
	"matricula" TEXT,
	"nombre" TEXT,
	"apellido" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id")	
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media2" (
	"id" INTEGER NOT NULL UNIQUE,
	"url" TEXT,
	"imagen" TEXT,
	"dibujo" TEXT,
	"matricula" TEXT,
	"nombre" TEXT,
	"estado" BOOLEAN NOT NULL,
	PRIMARY KEY("id")
);
