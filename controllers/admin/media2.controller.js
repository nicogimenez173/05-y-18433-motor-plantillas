const express = require ("express");
const router = express.Router();
const {db, getAll} = require('../../db/conexion');
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/images' })
const fileUpload = upload.single('src');
const sqlite3 = require('sqlite3');
const path = require('path');

const media2controller = {
    index: async function (req, res) {
        const media2 = await getAll("select * from media2");
    console.log("media2", media2);

    const medios = req.params.medios;
    console.log(medios)
    const integrantesFilter=await getAll ("select * from media2", [medios]);
    res.render('admin/media2/index',{
        media2:media2,
    });
    },

    create: async function (req, res) {
        res.render('admin/media2/crearForm');
    },

    store: async function (req, res) {
        try {
            const { id, url, dibujo, imagen, matricula, nombre, estado} = req.body;
            let imagenPath = '';
            let embedUrl = '';
            db.run(
                "INSERT INTO media2 (id, imagen, dibujo, matricula, nombre, estado) VALUES (?, ?, ?, ?, ?, ?)",
                [id, imagen, dibujo, matricula, nombre, estado],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/media2/listar");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },

    show: async function (req, res) {},

    update: async function (req, res) {
        const idmedia2 = parseInt(req.params.idmedia2);
        if (!req.body.url, !req.body.imagen,!req.body.dibujo,!req.body.matricula,!req.body.nombre) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        try {
            db.run("UPDATE media2 SET url = ?, dibujo = ?,imagen = ?, matricula = ?,nombre = ? WHERE id = ?",
                [
                    req.body.url,
                    req.body.imagen,
                    req.body.dibujo, 
                    req.body.matricula,
                    req.body.nombre,
                    idmedia2
                ]);
            res.json({success: true, message: 'Registro actualizado correctamente.'});
        } catch (error) {
            console.error('Error al actualizar el media:', error);
            res.status(500).json({error: '¡Error al actualizar al media!'});
        };

    },

    

    edit: async function (req, res) {
        try {
            const idmedia2 = parseInt(req.params.idmedia2);
            const media2 = await getAll('SELECT * FROM media2 WHERE id = ?', [idmedia2]);
            if (!media2) {
                return res.status(404).send('media no encontrado');
            }
            res.render('admin/media2/editform', {
                media2: media2
            });
        } catch (error) {
            console.error('Error al obtener el tipo media:', error);
            res.status(500).send('Error al obtener el tipo media');
        };


    },
    
    destroy: async function (req, res) {
        const idmedia2 = parseInt(req.params.idmedia2);
        console.log ("idmedia2", idmedia2);
        db.run(
            "UPDATE media SET estado = 0 WHERE id = ?",
            [idmedia2],
            (err) => {
                if (err) {
                    console.log ("error", err);                    
                }
                res.redirect('/admin/media2/listar?success=' + encodeURIComponent('¡Registro eliminado correctamente!'));
            }
        );

    },
};

module.exports = media2controller;