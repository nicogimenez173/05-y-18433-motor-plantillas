const express = require ("express");
const router = express.Router();
const {db, getAll} = require('../../db/conexion');
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: './public/images' })
const fileUpload = upload.single('src');
const sqlite3 = require('sqlite3');
const path = require('path');

/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/


const IntegrantesController = {
    index: async function (req, res) {
        const integrantes = await getAll("select * from integrantes Where estado = 1");
    console.log("INTEGRANTES", integrantes);
    const matricula = req.params.matricula;
    console.log(matricula)
        const integrantesFilter= await getAll ("select * from integrantes where matricula = ?", [matricula]);
        const media2Filter= await getAll ("select * from media2 where matricula = ?", [matricula]);
        res.render("admin/integrantes/index", {  
            integrantes:integrantes, 
        });
    },
    create: async function(req,res) {
        res.render('admin/integrantes/crearForm');

    },
    store: async function(req,res){
        try {
            const { id, matricula, nombre, apellido, estado } = req.body;
            db.run(
                "INSERT INTO integrantes (id, matricula, nombre, apellido, estado) VALUES (?, ?, ?, ?,?)",
                [id, matricula, nombre, apellido, estado],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/integrantes/listar");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        };
    },
   
   
   
    show:async function(req,res){

    },




    update:async function(req,res){
        const idintegrante = parseInt(req.params.idintegrante);
        if (!req.body.nombre || !req.body.apellido || !req.body.matricula) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        try {
            db.run("UPDATE integrantes SET nombre = ?, apellido = ?, matricula = ? WHERE id = ?",
                [
                    req.body.nombre,
                    req.body.apellido,
                    req.body.matricula,               
                    idintegrante
                ]);
            res.json({success: true, message: 'Registro actualizado correctamente.'});
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({error: '¡Error al actualizar al integrante!'});
        }



    },





    edit:async function(req,res){
        try {
            const idintegrante = parseInt(req.params.idintegrante);
            const integrantes = await getAll('SELECT * FROM integrantes WHERE id = ?', [idintegrante]);
            if (!integrantes) {
                return res.status(404).send('Integrante no encontrado');
            }
            res.render('admin/integrantes/editform', {
                integrantes: integrantes
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        };



        

    },




    destroy:async function(req,res){
        const idintegrante = parseInt(req.params.idintegrante);
        console.log ("idintegrante", idintegrante);
        
        db.run(
            "UPDATE integrantes SET estado = 0 WHERE id = ?",
            [idintegrante],
            (err) => {
                if (err) {
                    console.log ("error", err);                    
                }
                res.redirect('/admin/integrantes/listar?success=' + encodeURIComponent('¡Registro eliminado correctamente!'));
            } 
        )
    },
 };



 module.exports = IntegrantesController;